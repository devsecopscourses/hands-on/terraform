terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.26.0"
    }
  }

  backend "s3" {
    bucket         = "devsecops-courses-terraform"
    key            = "3-AWS-Backend-LockState/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "TerraformLock"
    encrypt        = true
  }
}

provider "aws" {
  region = "eu-east-1"
}
