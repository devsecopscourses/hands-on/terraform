# Terraform

Terraform is a powerful tool for building, modifying and versioning infrastructure securely and efficiently.

## Terraform installation on Linux

This documentation provides detailled instructions in order to install Terraform on Linux operating system. You can find full documentation [here](https://developer.hashicorp.com/terraform/install)

### Prerequisites

- Command Line Interface.
- Access to Internet.

### Installation

Open a terminal and type these commands

```bash
wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install terraform
```

## Terraform Basic Commands

After setting up your Terraform project with providers and resources, you'll primarily interact with Terraform through a set of basic commands. Each command plays a crucial role in managing your infrastructure.

### 1. **`terraform init`**

- **Purpose**: Initialize a Terraform working directory.
- **Description**: This command is used to initialize a directory containing Terraform configuration files. It installs necessary providers, prepares the state backend, and performs other initial setup tasks.
- **Files Created**: `terraform init` creates the `.terraform` directory, containing downloaded provider files and other data. This directory should not be committed to your Git repository as it contains environment-specific information.

### 2. **`terraform plan`**

- **Purpose**: Show an execution plan.
- **Description**: `terraform plan` creates an execution plan, showing what actions Terraform will perform when applying your configuration. This allows you to review the changes before actually applying them.
- **Importance**: This step is crucial for understanding the modifications that will be made to your infrastructure and avoiding potential surprises.

### 3. **`terraform apply`**

- **Purpose**: Apply configuration changes.
- **Description**: `terraform apply` applies the changes defined in your configuration files. It will create, modify, or delete infrastructure resources according to your execution plan.
- **Process**: Terraform will show the execution plan and ask for confirmation before applying the changes.

### 4. **`terraform destroy`**

- **Purpose**: Remove Terraform-managed infrastructure.
- **Description**: `terraform destroy` is used to remove the infrastructure managed by Terraform. This command destroys the Terraform-managed infrastructure resources as defined in your configuration files.
- **Usage**: This command is particularly useful in situations where you no longer need the infrastructure that you have deployed, such as at the end of a project, or when testing changes to your configuration.
- **Caution**: Before executing `terraform destroy`, it's crucial to ensure that you understand which resources will be affected. This action cannot be reversed and will result in the deletion of infrastructure resources.
- **Process**: Similar to `terraform apply`, this command will present an execution plan and require confirmation before proceeding. It shows you a list of all the resources that Terraform plans to destroy. After confirmation, Terraform will proceed to destroy the resources.
- **Best Practices**: Always review the destroy plan carefully to avoid unintended data loss or service disruption. In production environments, extra caution should be taken to avoid accidentally destroying critical infrastructure.

### 5. **`terraform fmt`**

- **Purpose**: Format configuration files.
- **Description**: `terraform fmt` is used to rewrite Terraform configuration files in a standardized and readable format. This helps maintain style consistency in your code.
- **Best Practices**: It is recommended to run this command regularly to keep your configuration files well-organized and readable.

## Best Practices with `.gitignore`

As mentioned, the `.terraform` directory and certain other files should not be committed to your Git repository. Here's a list of what you should include in your `.gitignore` file:

## Terraform File Conventions and Key Concepts

Terraform configurations are written in HashiCorp Configuration Language (HCL). Understanding the file naming conventions and key components of Terraform is essential for effective infrastructure management.

### File Naming Conventions

- **Provider Configuration**: Sometimes isolated in `providers.tf` for clarity, especially in larger configurations.
- **Main Configuration File**: Typically named `main.tf`. This file contains the primary configuration details for your Terraform module or project.
- **Variable Definitions**: Usually stored in files named `variables.tf`. These files define the variables used within the configuration.
- **Output Values**: Often defined in `outputs.tf`. This file declares output values that can be useful for displaying after running Terraform or for passing data to other Terraform configurations.
- **Resource Configuration**: While resources can be defined in `main.tf`, complex projects might separate them into specific files like `network.tf` or `compute.tf`, based on the type of resource.
- **Data Sources**: Defined in files typically named `datas.tf`. Data sources in Terraform are used to fetch and compute data from external sources, which can then be used elsewhere in the configuration.

### Data Sources in Terraform

Data sources in Terraform allow you to gather information from external resources or services. This information can be used to inform or configure other resources in your Terraform configuration.

#### Characteristics of Data Sources

- **Read-Only**: Data sources are read-only and are used to pull information from external sources into Terraform.
- **Dynamic Data Retrieval**: They are often used to dynamically fetch data like AMI IDs, VPC IDs, or DNS records.
- **Separation of Concerns**: Using data sources can help separate the definition of resources from the information required to configure them.

#### Usage Example

Here's an example of how you might use a data source to retrieve information about an AWS AMI:

```hcl
data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["canonical"] 

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-*-20.04-amd64-server-*"]
  }
}

resource "aws_instance" "example" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  # ... other configuration ...
}
```

### Key Concepts

#### Variables

- **Purpose**: Variables in Terraform are used to inject custom values into your Terraform configuration.
- **Example**:

```hcl
  variable "instance_type" {
    description = "EC2 instance type"
    default     = "t2.micro"
  }
```

## Hands On

### Basics providers

Terraform relies on plugins called "providers" to interact with remote systems. Providers are responsible for managing the lifecycle of resources, and each provider offers a set of resource types and data sources that it can manage.

#### Understanding Providers

Providers in Terraform can be thought of as a collection of resource types and data sources. For example, the `aws` provider offers resources such as `aws_instance` and `aws_vpc`. Similarly, providers like `random`, `local`, `null`, and `template` offer unique functionalities:

- **Random Provider**: Used for generating random values or strings, which can be useful for creating unique identifiers or passwords.
- **Local Provider**: Manages local resources such as local files. It's often used for generating files based on the Terraform configuration.
- **Null Provider**: Acts as a helper that does nothing but can be used to structure the Terraform configuration more logically.
- **Template Provider**: Used for template rendering, allowing the creation of dynamic content based on input variables.

### Example: Combining Providers

Let's consider an example where we combine the `random`, `local`, and `null` providers to perform a simple task:

1. **Random Provider**: Generates a random name.
2. **Local Provider**: Creates a file named `random-name.txt` and writes the random name into it.
3. **Null Provider**: Outputs the generated name using a `local-exec` provisioner.

#### Sample Configuration

Here is a snippet of Terraform code to demonstrate this:

```hcl
resource "random_pet" "name" {
  length    = 2
}

resource "local_file" "random_name_file" {
  content  = random_pet.name.id
  filename = "${path.module}/random-name.txt"
}

resource "null_resource" "display" {
  provisioner "local-exec" {
    command = "echo Generated Name: ${random_pet.name.id}"
  }
}
```

### Executing Terraform Code

After understanding the basics of providers in Terraform and seeing an example of combining different providers, the next step is to execute this Terraform code and explore further enhancements.

#### Running the Terraform Configuration

1. **Initialize Terraform**: Run `terraform init` in your project directory to initialize Terraform. This command prepares your project for execution.

2. **Review the Plan**: Execute `terraform plan` to review the changes that Terraform will make. This step does not make any actual changes to your infrastructure but shows what will happen when you apply the configuration.

3. **Apply the Configuration**: Use `terraform apply` to apply your configuration. Terraform will prompt you for confirmation before proceeding with the actual changes.

#### Exercice : Generating a Secure Password

In this exercise, you will enhance your Terraform configuration by adding functionality to generate a secure 32-character password. You will then use this password, along with a randomly generated name, to substitute values in a template file.

#### Task Overview

1. **Generate a Secure Password**:
   - Use the `random` provider to generate a 32-character password. This password should include special characters to ensure security.
   - **Hint**: Explore the `random_password` resource in the `random` provider documentation.

2. **Template File Substitution**:
   - Use the template file named `templates.d/example.tpl`.
   - In this template file, you can see placeholders for the random name and the generated password. Your task is to find out how to substitute these placeholders with the actual values generated by Terraform.

3. **Create an Output File**:
   - Using the rendered content from the template file, create a file named `myinsecurefile.txt` in an `outputs.d` directory.
   - This file should contain the substituted values for the random name and password.

#### Expected Outcome

At the end of this exercise, you should have:

- A Terraform configuration that generates a random name and a secure password.
- A template file that uses these generated values.
- An output file `myinsecurefile.txt` in the `outputs` directory with the substituted values.

This exercise will help you understand how to work with dynamic values in Terraform and the practical use of templating for configuration management. Good luck!

### Working with remote datas, variable files and workspace

#### Accessing Remote State Data

Terraform can access data stored in the state file of another Terraform project. This is useful for sharing data between projects or modules.

##### `terraform_remote_state` Usage

To access remote state, use the `terraform_remote_state` data source.

- **Specify the backend and the path to the state file**:

  ```hcl
  data "terraform_remote_state" "previous" {
    backend = "local"
    config = {
      path = "../path/to/previous/terraform.tfstate"
    }
  }

#### Understanding `locals` in Terraform

In Terraform, `locals` are used to simplify or make your configurations more readable. They serve as a way to assign names to expressions, allowing you to use these names instead of repeating the expressions.

##### Key Characteristics of `locals`

1. **Simplification**: Helps in simplifying complex expressions that are used multiple times within your configuration.
2. **Readability**: Improves the readability of your Terraform code.
3. **Reusability**: Allows for reusing expressions without duplicating the logic.

##### Usage Example of `locals`

Consider the following line in a Terraform configuration:

```hcl
locals {
  repeated_name = join("\n", formatlist("Retrieved Name: %s", [for i in range(var.random_pet_name_length): data.terraform_remote_state.previous.outputs.generated_name]))
}
```

In this example:

- `locals` is used to define a local value named `repeated_name`.
- `join` is a function that concatenates a collection of strings into a single string. Here, it joins multiple strings with a newline character (`\n`).
- `formatlist` formats a list of strings according to a given format. In this case, it formats each string to include "Retrieved Name: " followed by the value.
- The `[for i in range(var.random_pet_name_length): ...]` is a list comprehension that creates a list of strings. It repeats the string `data.terraform_remote_state.previous.outputs.generated_name` for a number of times equal to `var.random_pet_name_length`.
- The result is that `repeated_name` will hold a string where the name retrieved from the previous state is repeated `random_pet_name_length` times, each on a new line.

This approach demonstrates the power of `locals` in creating dynamic and reusable expressions within your Terraform configuration, making your code cleaner and more maintainable.

#### Using `.tfvars` for Variable Assignments

Terraform allows the use of `.tfvars` files to define variable values, which is particularly useful for managing different configurations for various environments, like development, staging, and production.

##### Creating a `.tfvars` File

Define your variables in a file with the `.tfvars` extension. For instance, you might have a `variables.tfvars` file with the following content:

```hcl
random_pet_name_length = 3
```

This file sets the random_pet_name_length variable to 3.

Applying Configuration with .tfvars
When applying your Terraform configuration, you can specify which .tfvars file to use with the -var-file flag. This enables you to easily switch between different sets of configurations. For example:

```bash
terraform apply -var-file="variables.tfvars"
```

Using `.tfvars` files enhances the flexibility and reusability of your Terraform configurations by allowing you to easily manage variable values for different scenarios or environments.

#### Understanding Terraform Workspaces

Workspaces in Terraform are used to manage different states of your infrastructure within the same configuration. This feature is particularly useful for maintaining separate environments such as development, staging, and production, under the same codebase.

##### Using Workspaces

By default, every Terraform configuration has a single workspace named `default`. However, you can create additional workspaces, each maintaining its own set of Terraform state.

##### Common Commands

- **List Workspaces**: `terraform workspace list` shows all available workspaces.
- **Create a New Workspace**: `terraform workspace new <workspace_name>` creates a new workspace.
- **Switch Workspaces**: `terraform workspace select <workspace_name>` switches to an existing workspace.
- **Delete a Workspace**: `terraform workspace delete <workspace_name>` deletes a workspace (Note: You cannot delete the `default` workspace).

Workspaces enable you to manage different infrastructure states separately, which is a best practice for complex Terraform implementations involving multiple environments.

#### Expected Outcome

At the end of this exercise, you should have:

- A Terraform configuration that retrieves a previously generated name from a Terraform state file.
- A `locals` block that dynamically creates a string, repeating the retrieved name a specified number of times, determined by the random_pet_name_length variable.
- A local file created by the `local_file` resource, with its filename based on the retrieved name and its content comprising the repeated name.
- An output that displays the retrieved name for verification.
- Familiarity with managing Terraform workspaces and variable files (.tfvars).

### Configuring AWS Backend for Terraform

This section covers the setup required for using AWS as a backend for Terraform, which includes using an S3 bucket for storing the state file and a DynamoDB table for state locking.

#### Prerequisites

Before you begin, ensure the following AWS resources are created and properly configured:

- **S3 Bucket**: An S3 bucket is needed to store the Terraform state file. The bucket should be created in the AWS region where you plan to deploy your resources. For this project, the bucket is named `devsecops-courses-terraform`.

- **DynamoDB Table**: A DynamoDB table is used for state locking to prevent concurrent executions of Terraform that might lead to state corruption. The table name used in this project is `TerraformLock`.

#### Provider and Backend Configuration

The `provider` and `backend` configuration in Terraform determines how Terraform interacts with AWS and where the state file is stored. Here's an example configuration:

```hcl
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.26.0"
    }
  }

  backend "s3" {
    bucket         = "devsecops-courses-terraform"
    key            = "3-AWS-Backend-LockState/terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "TerraformLock"
    encrypt        = true
  }
}

provider "aws" {
  region = "eu-west-1"
}
```

In this setup:

- The `required_providers` block specifies the AWS provider and its version.
- The `backend` "s3" block configures Terraform to use the specified S3 bucket and DynamoDB table. Ensure that the `bucket` and `dynamodb_table` values match the names of the resources you've created in AWS.
- The `region` should match the region of your S3 bucket and DynamoDB table.

#### Managing AWS Credentials for Terraform

To allow Terraform to manage resources in your AWS account, it needs to authenticate with AWS using an Access Key ID and Secret Access Key. There are several methods to safely provide these credentials to Terraform.

##### Method 1: Using Environment Variables

You can set your credentials as environment variables on the machine where you run Terraform. Terraform will automatically detect and use these credentials.

```bash
export AWS_ACCESS_KEY_ID="your_access_key"
export AWS_SECRET_ACCESS_KEY="your_secret_key"
```

Replace your_access_key and your_secret_key with your actual AWS credentials.

##### Method 2: AWS CLI Configuration

If you have the AWS CLI installed and configured, Terraform can use the credentials managed by it. Run the following command to configure the AWS CLI:

```bash
aws configure
```

Enter your Access Key ID, Secret Access Key, default region, and output format. Terraform will automatically use the default profile unless specified otherwise.

##### Method 3: Terraform AWS Provider Configuration

You can directly specify the credentials in the Terraform AWS provider block. However, this method is not recommended for production use due to security concerns.

```hcl
provider "aws" {
  region     = "eu-west-1"
  access_key = "your_access_key"
  secret_key = "your_secret_key"
}
```

##### Best Practices for Managing AWS Credentials

Security: Never hardcode your AWS credentials in your Terraform files, especially if you're sharing the code via version control systems like Git.
IAM Roles: For enhanced security, especially when running Terraform in an automated environment like a CI/CD pipeline, use IAM roles with appropriate permissions.
Variable Files: Avoid storing credentials in variable files that are checked into version control.

##### Note on IAM Permissions

Ensure the IAM user corresponding to the provided credentials has sufficient permissions to create and manage the resources defined in your Terraform configurations.

By following these practices, you can securely manage your AWS credentials while using Terraform for your infrastructure needs.

#### Exercice : Add Tags and public key to the instance

In this exercise, you will enhance your Terraform configuration by adding functionality to add some and also create a private key and public key for the EC2
