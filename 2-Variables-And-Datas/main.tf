locals {
  repeated_name = join("\n", formatlist("Retrieved Name: %s", [for i in range(var.random_pet_name_length) : data.terraform_remote_state.previous.outputs.generated_name]))
}

resource "local_file" "retrieved_name_file" {
  content  = local.repeated_name
  filename = "${path.module}/outputs/${data.terraform_remote_state.previous.outputs.generated_name}.txt"
}