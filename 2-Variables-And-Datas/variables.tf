variable "random_pet_name_length" {
  description = "Length of the random pet name"
  type        = number
  default     = 3
}