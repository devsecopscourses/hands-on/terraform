data "terraform_remote_state" "previous" {
  backend = "local"
  config = {
    path = "../1-Basics-Providers/terraform.tfstate"
  }
}