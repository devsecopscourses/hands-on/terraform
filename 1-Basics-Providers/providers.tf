terraform {
  required_providers {
    # The local provider is used for managing local resources. 
    # This could include creating local files or directories.
    local = {
      source  = "hashicorp/local"
      version = "2.4.0"
    }
    # The null provider is a rather special provider that allows you 
    # to implement behaviors and operations that don't fit into any other
    # provider. It's often used in conjunction with resources that
    # conditionally create other resources or to perform arbitrary logic
    # within your Terraform configurations
    null = {
      source  = "hashicorp/null"
      version = "3.2.1"
    }
    # The random provider is used to generate random values that can be
    # used by other resources. This could include random strings, numbers,
    # or even IDs
    random = {
      source  = "hashicorp/random"
      version = "3.5.1"
    }
    # The template provider allows you to template files or strings within
    # your Terraform configuration. This can be particularly useful 
    # for generating dynamic content, such as configuration files that 
    # need to include values known only at runtime
    template = {
      source  = "hashicorp/template"
      version = "2.2.0"
    }
  }
}
