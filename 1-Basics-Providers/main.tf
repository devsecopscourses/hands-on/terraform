resource "random_pet" "name" {
  length    = 2
  separator = "-"
}

data "template_file" "example" {
  template = "Generated name : ${random_pet.name.id}"
}

resource "local_file" "random_name_file" {
  content  = data.template_file.example.rendered
  filename = "${path.module}/random-name.txt"
}

resource "null_resource" "display" {
  triggers = {
    name = random_pet.name.id
  }

  provisioner "local-exec" {
    command = "echo Generated name : ${random_pet.name.id}"
  }
}