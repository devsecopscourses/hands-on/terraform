output "generated_name" {
  value = random_pet.name.id
}

output "file_path" {
  value = local_file.random_name_file.filename
}